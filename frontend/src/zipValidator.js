const usRe = /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/;
const caRe = /^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$/;

export function validateZipCode(country, zipCode){
    if (country == 'USA') {
        if(usRe.test(zipCode)){
          return true;
        }
    }
    else {
        if(caRe.test(zipCode)) {
          return true;
        }         
    }
    return false;
}
