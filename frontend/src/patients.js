import React, { Component } from 'react';
import { validateZipCode } from './zipValidator';


const USA = [
    'Alabama', 'Alaska', 'Arizona', 'Arkansas',
    'California', 'Colorado', 'Connecticut', 'Delaware',
    'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois',
    'Indiana', 'Iowa', 'Kansas', 'Kentucky',
    'Louisiana', 'Maine', 'Maryland', 'Massachusetts',
    'Michigan', 'Minnesota', 'Mississippi', 'Missouri',
    'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    'New Jersey', 'New Mexico', 'New York', 'North Carolina',
    'North Dakota', 'Ohio', 'Oklahoma', 'Oregon',
    'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota',
    'Tennessee', 'Texas', 'Utah','Vermont',
    'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

const CANADA = [
    'Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland and Labrador',
    'Northwest Territories', 'Nova Scotia', 'Nunavut', 'Ontario', 'Prince Edward Island', 
    'Quebec', 'Saskatchewan', 'Yukon'
];



class Patients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      country: 'Canada',
      province: CANADA[1],
      provinceList: CANADA,
      zipCode: '',
      postalClass: 'form-control',
      lowSyst: '0',
      lowSystClass: 'form-control',
      highSyst: '0',
      highSystClass: 'form-control',
      patients: [],
      formStatus: '',
      formStatusClass: '',
    };
    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleProvinceChange = this.handleProvinceChange.bind(this);
    this.handlezipCodeChange = this.handlezipCodeChange.bind(this);
    this.handlelowSystChange = this.handlelowSystChange.bind(this);
    this.handlehighSystChange = this.handlehighSystChange.bind(this);

  }
  
  componentDidMount(){
    this.setState({
      patients: this.props.patiens ?  this.props.patiens: []
    })
  }


  deletePatient(id, event) {
    const { patientService } = this.props;
    patientService.remove(id)
    let patients = this.state.patients;
    let newPatients = patients.filter( newPatient => newPatient.id !== id);
    this.setState({
      patients: newPatients
    })
  }
  
  handleSubmit(event) {
    const { patientService } = this.props;
    
    patientService.create({
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      country: this.state.country,
      province: this.state.province,
      zipCode: this.state.zipCode,
      lowSyst: this.state.lowSyst,
      highSyst: this.state.highSyst,
    })
    .then( result => {
      let patients = this.state.patients;
      patients.push(result);
      this.setState({
           firstName: '',
           lastName: '',
           email: '',
           country: 'Canada',
           province: CANADA[1],
           provinceList: CANADA,
           zipCode: '',
           postalClass: 'form-control',
           lowSyst: '0',
           lowSystClass: 'form-control',
           highSyst: '0',
           highSystClass: 'form-control',
           formStatus: '',
           formStatusClass: '',
        })
    })
    .catch( error => {
      this.setState({
        formStatus: error.message,
        formStatusClass: 'form-control is-invalid'
      });
    });

    event.preventDefault();
  }
  
  handleEmailChange(event){
    this.setState({email: event.target.value});
  }
  
  handleFirstNameChange(event){
    this.setState({firstName: event.target.value});
  }
  
  handleLastNameChange(event){
    this.setState({lastName: event.target.value});
  }
  
  handleCountryChange(event){
    let stateProvinces = [];
    let newProvince = '';
    stateProvinces = event.target.value === 'USA' ? USA: CANADA ;
    console.log( 'IN HANDLE CHANGE', event.target.value, stateProvinces)
    newProvince = stateProvinces[1];
    this.setState({
      country: event.target.value,
      provinceList: stateProvinces,
      province: newProvince,
      isZipValid: false,
      zipCode: ''
    });
    
  }
  
  handleProvinceChange(event){
    this.setState({province: event.target.value});
  }
  
  handlezipCodeChange(event){
    var postalClass = 'form-control'; 
    if(!validateZipCode( this.state.country, event.target.value )){
      postalClass += ' is-invalid'  
    }
    this.setState({
      zipCode: event.target.value,
      postalClass: postalClass
    });
  }
 
  handlelowSystChange(event){
    let systolicClass = 'form-control'; 
    if(event.target.value > Number(this.state.highSyst)){
      systolicClass +=  ' is-invalid';
    }
    this.setState(
      {lowSyst: event.target.value,
      lowSystClass: systolicClass,
      highSystClass: systolicClass
    });
  }
  
  handlehighSystChange(event){
    let systolicClass = 'form-control';
    if( event.target.value < Number(this.state.lowSyst) ){
      systolicClass += ' is-invalid';
    }
    this.setState({
      highSyst: event.target.value, 
      highSystClass: systolicClass,
      lowSystClass: systolicClass,
    });
  }

  render() {
      let stateProvinces = (this.state.provinceList).map( province => {
        return <option value={province} key={province}>{province}</option>
      });
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Patients</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} 
                className={this.state.formclass} 
                id="carform">
            
            <div className="form-group">
                <label htmlFor="nameInput">First Name:</label>
                <input  type="text" 
                        className="form-control" 
                        id="nameInput" 
                        placeholder="Enter your first name..." 
                        value={this.state.firstName}
                        onChange={this.handleFirstNameChange}
                        required
                        />
            </div>
            
            <div className="form-group">
              <label htmlFor="nameInput">Last Name:</label>
              <input  type="text"
                      className="form-control" 
                      id="nameInput" 
                      placeholder="Enter your last name..." 
                      value={this.state.lastName}
                      onChange={this.handleLastNameChange}
                      required
                      />
            </div>
            
            <div className="form-group">
              <label htmlFor="email">Email Address</label>
              <input  type="email" 
                      className="form-control" 
                      id="email" 
                      aria-describedby="emailHelp" 
                      placeholder="Enter email here"
                      value={this.state.email}
                      onChange={this.handleEmailChange}
                      required
                      />
               <div id="email-valid" className="invalid-feedback">Email was already registered.</div>
            </div>
            
            
           <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">Country:</label>
              <select className="form-control" 
                      id="exampleFormControlSelect1"
                      value={this.state.country}
                      onChange={this.handleCountryChange}
                      required
                >
                  <option>Canada</option>
                  <option>USA</option> 
              </select>
            </div>
            
           <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">State:</label>
              <select className="form-control" 
                      id="exampleFormControlSelect1"
                      value={this.state.province}
                      onChange={this.handleProvinceChange} >
              {stateProvinces}
              </select>
            </div>
            
            <div className="form-group">
              <label htmlFor="zipCode">Postal Code:</label>
              <input  type="text" 
                      className={this.state.postalClass}
                      id="lowSyst" 
                      aria-describedby="emailHelp" 
                      placeholder="Zip Code.."
                      value={this.state.zipCode}
                      onChange={this.handlezipCodeChange}
              />
                <div className="invalid-feedback">
                  You must enter a valid postal code for the selected country.
                </div>
            </div>
            
            <div className="form-group">
              <label htmlFor="lowSyst">Low Systolic Pressure:</label>
              <input  type="number" 
                      className={this.state.lowSystClass}
                      id="lowSyst" 
                      value={this.state.lowSyst}
                      onChange={this.handlelowSystChange}
                      />
                <div className="invalid-feedback">
                  Low systolic blood pressure must be <u>lower</u> than the high systolic blood pressure.    
                </div>
            </div>
            
            <div className="form-group">
              <label htmlFor="highSyst">High Systolic Pressure:</label>
              <input  type="number" 
                      className={this.state.highSystClass}
                      id="highSyst" 
                      value={this.state.highSyst}
                      onChange={this.handlehighSystChange}
                      />
                <div className="invalid-feedback">
                  High systolic blood pressure must be <u>higher</u> than the high systolic blood pressure.    
                </div>
            </div>
            
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add patient</button>
            <div className={this.state.formStatusClass}>
                {this.state.formStatus} 
            </div>
          </form>
        </div>
      </div>
      <div className="table-responsive">
         <table className="table table-bordered table-hover">
             <thead className="patientTable">
             <tr>
                 <th>First Name</th>
                 <th>Last Name</th>
                 <th>Email</th>
                 <th>Country</th>
                 <th>Province/State</th>
                 <th>Postal Code</th>
                 <th>Low Systolic</th>
                 <th>High Systolic</th>
                 <th>Delete Patient</th>
               </tr>
             </thead>
           <tbody>
             {
               this.state.patients.map( patient => {
                 return (
                   <tr key={patient.firstName}>
                     <td>{patient.firstName}</td> 
                     <td>{patient.lastName}</td> 
                     <td>{patient.email}</td> 
                     <td>{patient.country}</td> 
                     <td>{patient.province}</td> 
                     <td>{patient.zipCode}</td> 
                     <td>{patient.lowSyst}</td> 
                     <td>{patient.highSyst}</td> 
                     <td><button onClick={this.deletePatient.bind(this, patient.id)} type="button" className="btn btn-danger">Delete</button></td> 
                   </tr>
                 )
               })
             }
           </tbody>
         </table>
      </div>
      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Patients;