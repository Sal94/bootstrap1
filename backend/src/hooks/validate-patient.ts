// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, data } = context;
    
    if(!data.firstName){
      throw new Error('First name for patient needed.');
    };
    const firstName = data.firstName;
    
    if(!data.lastName){
      throw new Error('Last name for patient needed.');
    };
    const lastName = data.lastName;
    
    if(!data.email){
      throw new Error('Email is needed.');
    };
    const email = data.email;
    
    if(!data.country){
      throw new Error('Country is needed.');
    };
    const country = data.country;

    if(!data.province){
      throw new Error('Province is needed.');
    };
    const province = data.province;
    
    if(!data.zipCode){
      throw new Error('Zip Code is needed.');
    };
    const zipCode = data.zipCode;
    
    if(!data.lowSyst && !data.highSyst){
      throw new Error('Low/High systolic pressure is needed.');
    }

    const lowSyst = data.lowSyst;
    const highSyst = data.highSyst;
    
    if(Number(lowSyst) > Number(highSyst)){
      throw new Error('Low systolic pressure cannot be higher than the high one.');
    }
    
    const emails = await app.service('patients').find({
      query: {
        email: email
      }
    });
    
    if(emails.total > 0){
      throw new Error('Email already exists');
    };
    
    context.data = {
      firstName,
      lastName,
      email,
      country,
      province,
      zipCode,
      lowSyst,
      highSyst,
    }
    
    return context;
  };
}
